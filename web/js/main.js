jQuery(function() {
	initTabs();
	initMobileNav();
	initAddClasses();
	initSlideShow();
	initFancybox();
	initRetinaCover();
	initCustomForms();
	initAccordion();
	initOpenClose();
	initSlickCarousel();
	initAnchors();
	initStickyScrollBlock();
	initPopupAppears();
});

// initialize popup appears
function initPopupAppears() {
	var popupLink = jQuery('#timing-opener');

	if (popupLink.length) {
		setTimeout(function() {
			popupLink.trigger('click');
		}, popupLink.data('time') || 1000);
	}
}

// initialize fixed blocks on scroll
function initStickyScrollBlock() {
	ResponsiveHelper.addRange({
		'992..': {
			on: function() {
				jQuery('.fiexed-slider-wrap').stickyScrollBlock({
					setBoxHeight: true,
					activeClass: 'fixed-position',
					container: '.product-details-section',
					positionType: 'fixed',
					extraTop: function() {
						var totalHeight = 0;
						jQuery('0').each(function() {
							totalHeight += jQuery(this).outerHeight();
						});
						return totalHeight;
					}
				});
			},
			off: function() {
				jQuery('.fiexed-slider-wrap').stickyScrollBlock('destroy');
			}
		}
	});
}


// initialize smooth anchor links
function initAnchors() {
	var anchorInstance1;
	ResponsiveHelper.addRange({
		'992..': {
			on: function() {
				anchorInstance1 = new SmoothScroll({
					anchorLinks: 'a.anchor-details',
					extraOffset: 0,
					wheelBehavior: 'none'
				});
			},
			off: function() {
				anchorInstance1.destroy();
			}
		}
	});
}

// slick init
function initSlickCarousel() {
	jQuery('.product-slick-slider').slick({
		slidesToScroll: 1,
		rows: 0,
		prevArrow: '<button class="slick-prev"></button>',
		nextArrow: '<button class="slick-next"></button>',
		asNavFor: '.product-slider-nav'
	});
	
	jQuery('.product-slider-nav').slick({
		slidesToScroll: 1,
		rows: 0,
		slidesToShow: 4,
		arrows: false,
		asNavFor: '.product-slick-slider',
		focusOnSelect: true,
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToScroll: 1,
				slidesToShow: 3
			}
		}]
	});
}

// open-close init
function initOpenClose() {
	ResponsiveHelper.addRange({
		'992..': {
			on: function() {
				jQuery('.menu-open-close').openClose({
					activeClass: 'active',
					opener: '.aside-menu-opener',
					slider: '.aside-open-slide',
					animSpeed: 200,
					effect: 'slide'
				});
			},
			off: function() {
				jQuery('.menu-open-close').openClose('destroy');
			}
		}
	});

	ResponsiveHelper.addRange({
		'..992': {
			on: function() {
				jQuery('.aside-mobile-open-close').openClose({
					activeClass: 'aside-mobile-active',
					opener: '.aside-mobile-opener',
					slider: '.aside-mobile-opener-slide',
					animSpeed: 200,
					effect: 'slide'
				});
			},
			off: function() {
				jQuery('.aside-mobile-open-close').openClose('destroy');
			}
		}
	});

	ResponsiveHelper.addRange({
		'..992': {
			on: function() {
				jQuery('.catalog-section-open-close-wrap').openClose({
					activeClass: 'active-aside',
					opener: '.aside-bar-opener, .aside-inner-close',
					slider: '.aside-bar-slide',
					hideOnClickOutside: true,
					effect: 'none'
				});
			},
			off: function() {
				jQuery('.catalog-section-open-close-wrap').openClose('destroy');
			}
		}
	});
}

// accordion menu init
function initAccordion() {
	jQuery('.menu-accordion').slideAccordion({
		opener: '.menu-opener',
		slider: '.menu-slide',
		animSpeed: 200
	});

	ResponsiveHelper.addRange({
		'..992': {
			on: function() {
				jQuery('.details-accordion').slideAccordion({
					opener: '.accordion-opener',
					slider: '.details-accordion-slide',
					animSpeed: 300
				});
			},
			off: function() {
				jQuery('.details-accordion').slideAccordion('destroy');
			}
		}
	});
}

// initialize custom form elements
function initCustomForms() {
	jcf.setOptions('Select', {
		wrapNative: false,
		wrapNativeOnMobile: false,
		fakeDropInBody: false,
	});
	jcf.replaceAll();
}

function initRetinaCover() {
	jQuery('.bg-stretch').retinaCover();
}

// lightbox init
function initFancybox() {
	jQuery('a.lightbox, [data-fancybox]').fancybox({
		parentEl: 'body',
		margin: [50, 0]
	});

	jQuery('a.popup-side-link').fancybox({
		parentEl: 'body',
		margin: [50, 0],
		smallBtn: true,
		loop: false,
		infobar: false,
		slideShow: false,
		fullScreen: false,
		thumbs: false,
		toolbarCloseBtn: false,
		baseClass: 'popup-side-wrap',
		touch: false
	});
}

// fade gallery init
function initSlideShow() {
	jQuery('.hero-slideshow').fadeGallery({
		slides: '.slide',
		btnPrev: '.btn-prev',
		btnNext: '.btn-next',
		autoRotation: true
	});
}

// add class on click
function initAddClasses() {
	jQuery('.main-nav-list a, .nav-tabset-wrap').clickClass({
		classAdd: 'nav-drop-active',
		addToParent: 'main-nav',
		event: 'mouseenter mouseleave'
	});

	jQuery('.select-opener').clickClass({
		classAdd: 'active-select',
		addToParent: 'select-open-holder'
	});
}

// content tabs init
function initTabs() {
	ResponsiveHelper.addRange({
		'..991': {
			on: function() {
				jQuery('.nav-tabset').tabset({
					tabLinks: 'a',
					activeClass: 'active-tab',
					defaultTab: true
				});
			},
			off: function() {
				jQuery('.nav-tabset').tabset('destroy');
			}
		}
	});
}

// mobile menu init
function initMobileNav() {
	ResponsiveHelper.addRange({
		'..': {
			on: function() {
				jQuery('#header').mobileNav({
					menuActiveClass: 'nav-active',
					menuOpener: '.nav-opener'
				});
			},
			off: function() {
				jQuery('#header').mobileNav('destroy');
			}
		}
	});
}
